# Hashicorp Vault

This role will install and start a Vault server on a single machine. It
currently does not support creating a cluster.

TLS is configured as disabled on the vault server in this config. This may be
changed in the future, however we are utilizing an encrypted overlay for all of
our services.
